import os
import shutil
import time
import random
import numpy as np
import stonesoup.metricgenerator.ospametric as smo
import stonesoup.types.array as sta
import stonesoup.types.state as sts
import scipy.stats
import matplotlib.pyplot as plt
from itertools import combinations
from gen_test_cases import gen_static, gen_dynamic, gen_centers, gen_dynamic_2, gen_center_hist, get_center_length
import cadquery as cq
from cadquery import exporters
from cad_helper import get_rect, get_circle, get_angle

from filterpy.kalman import KalmanFilter
from filterpy.common import Q_discrete_white_noise


# from t2ta_sampling import t2ta_stochastic_optimization


# for every object and sensor we need a Tracker (KF)
class Tracker:
    def __init__(self, measurements, init_point, sigma_sensor=np.eye(2)):
        self.pos = measurements[0]
        # initialize KF with state dimension (x) und measurement dimension (z)
        self.kf = KalmanFilter(dim_x=4, dim_z=2)
        self.init_kf(init_point[:2], measurements, sigma_sensor)

    def init_kf(self, init_point, measurements, sigma_sensor):
        # initialise state
        self.kf.x = np.hstack((init_point, measurements[1][:2] - measurements[0][:2]))
        # define state transition matrix
        self.kf.F = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]])
        # define measurement matrix
        self.kf.H = np.array([[1, 0, 0, 0], [0, 1, 0, 0]])
        # define uncertainty -> P is already np.eye(x_dim)
        self.kf.P *= 700
        # define measurement noise
        self.kf.R = sigma_sensor
        # define process noise
        g = np.array([0.5, 0.5, 1., 1.])
        self.kf.Q = g @ g.T * 1  # Q_discrete_white_noise(dim=2, dt=1, var=1, block_size=2)

    def get_estimate(self, measurement):
        self.kf.predict()
        self.kf.update(measurement)
        self.pos[:2] = self.kf.x[:2]
        # append velocity
        self.pos = np.hstack((self.pos, self.kf.x[2:]))

        return np.array(self.pos)


class StochasticT2TA:
    def __init__(self, tracks, cnt_sensor, cnt_samples, detect_prob, debug_centers, cov_sensor):
        self.tracks = tracks
        self.cnt_tracks = tracks.shape[0]  # get the number of tracks
        self.cnt_sensor = cnt_sensor
        self.cnt_samples = cnt_samples
        self.debug_centers = debug_centers
        # maximum detect prob should be 0.97 since bigger values cause problem during likelihood calculation
        if detect_prob > 0.97:
            self.detect_prob = 0.97
        else:
            self.detect_prob = detect_prob

        # we have cnt_samples * cnt_tracks associations and every association has length cnt_tracks
        self.all_ass = np.zeros((self.cnt_samples * self.cnt_tracks, self.cnt_tracks), dtype=int)
        self.weights = np.ones(self.all_ass.shape[0])
        self.best_ass = 0  # index for best association in self.all_ass
        self.cov_sensor = cov_sensor

    def calc_t2ta(self, split=False):
        cnt_cluster = self.cnt_tracks  # next cluster ID

        clusters = [{i} for i in range(self.cnt_tracks)]  # create cluster: every cluster saves the t_idx of the tracks
        counter = 0
        tmp_ass = np.arange(self.cnt_tracks)  # initially every track has its own cluster 0 based counting
        for i in range(self.cnt_samples):

            for t_idx, t in enumerate(
                    self.tracks):  # t_idx gives index of track, so we know where to look for it in assc list

                cluster_likelihood = [self.calc_likelihood_cluster(c) for c in clusters]

                actions = [0. for _ in range(2 * cnt_cluster + 2)]  # actions are specified by indices
                actions[0] = 1.0
                t_cluster = tmp_ass[t_idx]  # get cluster for current track
                # likelihood of cluster with current track
                w_cluster_t = cluster_likelihood[t_cluster]
                # likelihood of cluster without current track
                w_cluster_without_t = self.calc_likelihood_cluster(clusters[t_cluster].difference({t_idx}))
                # calc p_s
                actions[1] = self.calc_move_to_single(clusters[t_cluster], t_idx, w_cluster_t, w_cluster_without_t)
                if split:
                    splits, weights = self.calc_split_cluster(clusters[t_cluster], w_cluster_t)
                    actions.extend(weights)
                for c_idx, c in enumerate(clusters):
                    # calc likelihood of cluster
                    w_c = cluster_likelihood[c_idx]
                    # calc p^m_c

                    tmp1 = self.calc_move_to_cluster(c, t_idx, w_cluster_t, w_cluster_without_t, w_c)
                    actions[2 + c_idx] = tmp1  # index should be checked
                    # calc p^M_c
                    tmp2 = self.calc_merge_cluster(clusters[t_cluster], c, w_c, w_cluster_t)
                    actions[2 + cnt_cluster + c_idx] = tmp2  # index should be checked

                # normalize actions by dividing through the sum of all actions
                # s = np.sum(actions)
                actions_norm = actions / np.sum(actions)
                # actions_norm = [x / s for x in actions]
                # choose action
                action = np.random.choice(range(len(actions_norm)), p=actions_norm)

                # cluster that should be removed
                cluster_to_remove = -1

                # action == p_s --> move current track to new cluster
                if action == 1:
                    # create new cluster with only current track
                    clusters.append({t_idx})
                    # remove current track from its former cluster
                    clusters[t_cluster].remove(t_idx)
                    # change cluster for current track in associtation
                    tmp_ass[t_idx] = cnt_cluster
                    # increment cluster count
                    cnt_cluster += 1
                # action == p^m_c --> move current track to cluster with idx action
                elif 1 < action < cnt_cluster + 2:
                    # remove current track from its former cluster
                    clusters[t_cluster].remove(t_idx)
                    if not clusters[t_cluster]:
                        cluster_to_remove = t_cluster
                        cnt_cluster -= 1

                    # add current track to its new cluster
                    clusters[action - 2].add(t_idx)
                    # change cluster for current track in associtation
                    tmp_ass[t_idx] = action - 2
                # action == p^M_c
                elif cnt_cluster + 2 <= action < 2 * cnt_cluster + 2:
                    # union both clusters
                    tmp_ass[list(clusters[action - cnt_cluster - 2])] = t_cluster
                    clusters[t_cluster] = clusters[t_cluster].union(clusters[action - cnt_cluster - 2])
                    cluster_to_remove = action - cnt_cluster - 2
                    cnt_cluster -= 1

                elif split and 2 * cnt_cluster + 2 <= action:
                    new_c = splits[action - (2 * cnt_cluster + 2)]
                    # create new cluster with only current track
                    clusters.append(new_c)
                    # remove current track from its former cluster
                    clusters[t_cluster] -= new_c
                    # change cluster for current track in associtation
                    tmp_ass[list(new_c)] = cnt_cluster
                    # increment cluster count
                    cnt_cluster += 1

                # remove emtpy clusters
                self.remove_cluster(tmp_ass, cluster_to_remove, clusters)
                self.all_ass[counter] = tmp_ass  # save association after every step
                counter += 1
        self.calc_best_ass()

    def sanitize_ass(self, ass):
        res = ass.copy()
        for i in range(len(set(res))):
            c_id = res[np.nonzero(res >= 0)[0][0]]
            res[res == c_id] = -i - 1
        res += 1
        return -res

    def calc_best_ass(self):
        for i, a in enumerate(self.all_ass):

            for c in set(a):
                idx = set(np.where(a == c)[0])
                tmp = self.calc_likelihood_cluster(idx)
                self.weights[i] += np.log(tmp)

        self.best_ass = self.weights.argmax()

    @staticmethod
    def remove_cluster(assc, to_remove, clusters):
        if to_remove == -1:
            return
        del clusters[to_remove]
        for i, v in enumerate(assc):
            if v > to_remove:
                assc[i] -= 1

    # weight calculation should somehow be implemented with sets
    def calc_move_to_single(self, t_cluster, t_id, w_cluster_t, w_cluster_without_t):
        if len(t_cluster) == 1:
            return 0

        t_single = self.calc_likelihood_cluster({t_id})

        return (t_single * w_cluster_without_t) / w_cluster_t

    def calc_move_to_cluster(self, c_o, t_id, w_cluster_t, w_cluster_without_t, w_c_o):

        # check if cluster already contains track from sensor of current track
        if set(self.tracks[list(c_o), 2]).intersection({self.tracks[t_id][2]}):
            # print(f'{c_o=}, {t_id=}')
            # print(f'{self.tracks[list(c_o)][:, 2]}, {self.tracks[t_id][2]}')
            # print('hi\n----------------------------------------------------------------------------------------')
            return 0
        # print('hallo')
        w1 = self.calc_likelihood_cluster(c_o | {t_id})
        return (w1 * w_cluster_without_t) / (w_cluster_t * w_c_o)

    def calc_merge_cluster(self, c_t, c_o, w_c_o, w_c_t):

        if not (len(c_t) > 1 and set(self.tracks[list(c_o), 2]).isdisjoint(set(self.tracks[list(c_t), 2]))):
            # print(f'{c_o=}, {c_t=}')
            # print(f'{self.tracks[list(c_o)][:, 2]}, {self.tracks[list(c_t)][:, 2]}')
            # print('hi\n----------------------------------------------------------------------------------------')
            return 0
        # print('hallo')
        w1 = self.calc_likelihood_cluster(c_o | c_t)

        return w1 / (w_c_o * w_c_t)

    def calc_split_cluster(self, c_t, w_c_t):
        splits = []
        weights = []
        if len(c_t) == 1:
            return 0, set()

        for i in range(1, len(c_t) - 1):
            for subset in combinations(c_t, i):
                subset = set(subset)
                w_subset = self.calc_likelihood_cluster(subset)
                w_diff = self.calc_likelihood_cluster(c_t - subset)

                weights.append((w_subset * w_diff) / w_c_t)
                splits.append(subset)

        return splits, weights

    def calc_likelihood_cluster(self, cluster):

        if not cluster:
            return 1

        mean = np.mean(self.tracks[list(cluster), :2], axis=0)  # approximated mean of cluster
        sigma = (1 + 1 / len(cluster)) * self.cov_sensor  # covariance
        w_c = self.detect_prob ** len(cluster) * (1 - self.detect_prob) ** (self.cnt_sensor - len(cluster))
        res = scipy.stats.multivariate_normal.pdf(self.tracks[list(cluster)][:, :2], mean=mean, cov=sigma)
        # res = self.gaussian_likelihood(mean, sigma, self.tracks[list(cluster), :2])
        res = np.prod(res)

        return w_c * res

    def plot_associtation(self, step, test_case, t=0.0, split=False):
        g = self.get_gospa()[0].value['distance']
        cnt_cent = int(self.tracks.shape[0] / np.unique(self.tracks[:, 2]).shape[0])
        best_ass = self.all_ass[self.best_ass]
        fig, ax = plt.subplots(2, 1, figsize=(8, 8))

        for i in range(2):
            ax[i].set_xticks([])
            ax[i].set_yticks([])
            ax[i].scatter(self.debug_centers[:, 0], self.debug_centers[:, 1], s=100, c='red', marker='x')

        ax[0].set_title(f'Best Association, #sensors: {self.cnt_sensor}, #centers: {cnt_cent}, GOSPA: {g},\n time: {t}')
        ax[1].set_title(f'Ground Truth, noise: {self.cov_sensor}')

        ax[0].scatter(self.tracks[:, 0], self.tracks[:, 1], c=best_ass, cmap='tab20')
        ax[1].scatter(self.tracks[:, 0], self.tracks[:, 1], c=self.tracks[:, 3], cmap='tab20')
        if split:
            fig.savefig(f'./Plots/Tests/TestCase{test_case}/step{step}_split.png')
        else:
            fig.savefig(f'./Plots/Tests/TestCase{test_case}/step{step}.png')
        plt.close(fig)
        return g

    def get_gospa(self):
        g = smo.GOSPAMetric(p=1, c=10)
        cnt_cent = int(self.tracks.shape[0] / np.unique(self.tracks[:, 2]).shape[0])

        truths_state2 = [sts.State(sta.StateVector(np.sum(self.tracks[i::cnt_cent, :2], axis=0) / self.cnt_sensor))
                         for i in range(cnt_cent)]

        best_ass = self.all_ass[self.best_ass]
        cnt_cluster = max(best_ass) + 1
        _, cluster_sizes = np.unique(best_ass, return_counts=True)

        approx_centers = np.array([[0., 0.] for _ in range(cnt_cluster)])

        for i, t in enumerate(self.tracks):
            approx_centers[best_ass[i]] += (t[:2] / cluster_sizes[best_ass[i]])

        estimates = [sts.State(sta.StateVector(t)) for t in approx_centers]

        res = g.compute_gospa_metric(estimates, truths_state2)
        return res

    def get_best_ass(self):
        return self.all_ass[self.best_ass]


def create_dir():
    if os.path.isdir(f'./Plots/Tests'):
        shutil.rmtree(f'./Plots/Tests')
    os.mkdir(f'./Plots/Tests')
    os.mkdir(f'./Plots/Tests/GOSPAS')


def static_test(testcases, mean):
    # check if directory is present and delete it
    create_dir()
    for i in range(testcases):
        os.mkdir(f'./Plots/Tests/TestCase{i}')
        # choose random values for test
        cnt_sensor = 15  # random.randint(3, 15)
        cnt_centers = 15  # random.randint(4, 10)
        # generate random covariance
        tmp = np.random.uniform(0.3, 1.5)
        # we give the eigenvalues and they should sum up to the desired dimensionality
        cov = scipy.stats.random_correlation.rvs((tmp, 2 - tmp))
        centers = gen_centers(cnt_centers, [0., 0.])
        ts, centers = gen_static(centers, cnt_sensors=cnt_sensor,
                                 sigma_sensor=cov)

        assco = StochasticT2TA(ts, cnt_sensor=cnt_sensor, cnt_samples=100, detect_prob=0.97, debug_centers=centers,
                               cov_sensor=cov)
        assco.calc_t2ta()
        assco.plot_associtation(0, i)
    print('finished')


def gen_rand_cov():
    # generate random covariance
    tmp = np.random.uniform(0.3, 1.5)
    # generate random scalar for cov matrix
    c = random.randint(1, 10)
    cov = scipy.stats.random_correlation.rvs((tmp, 2 - tmp))
    cov *= c
    return cov


def dynamic(run, centers=None, cnt_sensor=None, cnt_centers=None, cnt_points=None, use_args=False, same_vel=False,
            use_split=True):
    metas = [{} for _ in range(cnt_points + 1)]
    os.mkdir(f'./Plots/Tests/TestCase{run}')

    if not use_args:
        # if no args are given choose random parameter
        cnt_sensor = random.randint(5, 7)
        cnt_centers = random.randint(5, 7)
        # we give the eigenvalues and they should sum up to the desired dimensionality
        centers = gen_centers(cnt_centers, [0., 0.])

    # generate random covariance
    cov = gen_rand_cov()
    if not same_vel:
        data = gen_dynamic(centers, cnt_sensors=cnt_sensor, cnt_points=cnt_points,
                           sigma_sensor=cov)
    else:
        data = gen_dynamic_2(centers, cnt_sensors=cnt_sensor)
    # init tracker --> sublist: one sensor all centers
    tracker = [Tracker([data[0][0][i * cnt_centers + j], data[1][0][i * cnt_centers + j]], data[0][1][j], cov)
               for i in range(cnt_sensor) for j in range(cnt_centers)]

    times, times_split = np.zeros(cnt_points + 1), np.zeros(cnt_points + 1)
    gospas, gospas_split = np.zeros(cnt_points + 1), np.zeros(cnt_points + 1)
    for i, d in enumerate(data):
        measurements = []
        ts, centers = d
        # we take the first two measurements directly
        if i < 2:
            measurements = ts
        else:
            for t, m in zip(tracker, ts):
                measurements.append(t.get_estimate(m[:2]))
            measurements = np.array(measurements)
        assco_1 = StochasticT2TA(measurements, cnt_sensor=cnt_sensor, cnt_samples=1, detect_prob=0.97,
                                 debug_centers=centers, cov_sensor=cov)

        start = time.time()
        assco_1.calc_t2ta()
        end = time.time()
        times[i] = end - start
        meta = {'asso': assco_1.get_best_ass(), 'tracks': assco_1.tracks, 'truths': assco_1.debug_centers}
        metas[i] = meta
        g = assco_1.plot_associtation(i, run, end - start)
        gospas[i] = g

        if use_split:
            assco_2 = StochasticT2TA(measurements, cnt_sensor=cnt_sensor, cnt_samples=1, detect_prob=0.97,
                                     debug_centers=centers, cov_sensor=cov)
            start = time.time()
            assco_2.calc_t2ta(split=use_split)
            end = time.time()
            g = assco_2.plot_associtation(i, run, end - start, split=use_split)
            gospas_split[i] = g
            times_split[i] = end - start

    fig, ax = plt.subplots(2, 1, figsize=(8, 8))
    xs = [i for i in range(cnt_points + 1)]
    for i, v in enumerate(zip([gospas, times], ['GOSPA', 'Time'])):
        data, title = v
        ax[i].plot(xs, data)
        ax[i].set_xlabel('Time Step')
        ax[i].set_ylabel(f'{title}')
        ax[i].set_title(f'{title}: #Sensor: {cnt_sensor}, #objects: {cnt_centers},\n Sensor noise: {cov}')

    if use_split:
        ax[0].plot(xs, gospas_split, c='red')
        ax[1].plot(xs, times_split, c='red')
    ax[0].legend(['No Split', 'Split'], loc='upper right')
    ax[1].legend(['No Split', 'Split'], loc='upper right')
    plt.tight_layout()
    fig.savefig(f'./Plots/Tests/GOSPAS/GOSPA_RUN{run}.png')
    plt.close(fig)
    return gospas, gospas_split, metas


def montecarlo_test(test_cases, mean, cnt_points=10, same_vel=False, use_split=False):
    create_dir()
    # choose random values for test
    cnt_sensor = random.randint(7, 9)
    cnt_centers = random.randint(7, 9)
    metas = []
    if not same_vel:
        centers = gen_centers(cnt_centers, [0., 0.])
    else:
        centers = gen_center_hist(cnt_centers, cnt_points, mu=[0., 0.])
    gospas, gospas_split = np.zeros((test_cases, cnt_points + 1)), np.zeros((test_cases, cnt_points + 1))
    # for every step
    for j in range(test_cases):
        g, gs, ms = dynamic(run=j, centers=centers, cnt_centers=cnt_centers, cnt_sensor=cnt_sensor,
                            cnt_points=cnt_points,
                            use_args=True, same_vel=same_vel, use_split=use_split)
        metas.append(ms)
        gospas[j] = g
        gospas_split[j] = gs

    gospas = np.mean(gospas, axis=0)
    gospas_split = np.mean(gospas_split, axis=0)

    fig, ax = plt.subplots(figsize=(8, 8))
    xs = [i for i in range(cnt_points + 1)]
    ax.plot(xs, gospas)
    ax.plot(xs, gospas_split, c='red')
    ax.legend(['No Split', 'Split'], loc='upper right')
    ax.set_xlabel('Time Step')
    ax.set_ylabel(f'Mean Gospa')
    ax.set_title(f'Mean Gospa: #Sensor: {cnt_sensor}, #objects: {cnt_centers}')

    plt.tight_layout()
    fig.savefig(f'./Plots/Tests/GOSPAS/GOSPA_Mean.png')
    plt.close(fig)

    print('finished')
    return metas


def dynamic_test(test_cases, mean, cnt_points=15):
    # check if directory is present and delete it
    create_dir()
    # for every step
    for j in range(test_cases):
        dynamic(run=j, cnt_points=cnt_points)
    print('finished')


def cad_associtation(asso, tracks, truths):
    cs = plt.get_cmap('tab20c').colors
    cs = cs * 20
    cnt_cluster = max(asso) + 1
    _, cluster_sizes = np.unique(asso, return_counts=True)

    approx_centers = np.array([[0., 0.] for _ in range(cnt_cluster)])
    approx_veloc = np.array([[0., 0.] for _ in range(cnt_cluster)])

    assy = cq.Assembly()

    for i, t in enumerate(tracks):
        approx_centers[asso[i]] += (t[:2] / cluster_sizes[asso[i]])
        approx_veloc[asso[i]] += (t[-2:] / cluster_sizes[asso[i]])
        x = cq.Workplane("XY").text("X", 1.5, 0.5)
        x = x.translate((t[0], t[1], 0))
        assy.add(x, color=cq.Color(*cs[asso[i]]), name=f'x{i}')

    for i, cv in enumerate(zip(approx_centers, approx_veloc)):
        c, v = cv
        a = get_angle(v, [1, 0])

        l1 = random.randint(3, 5)
        assy.add(get_rect((c[0], c[1], l1), a), color=cq.Color(*cs[i]), name=f'rect{i}')

    for i, c in enumerate(truths):
        a = get_angle(c[-2:], [1, 0])
        l1 = random.randint(3, 5)
        assy.add(get_rect((c[0], c[1], l1), a), color=cq.Color(1, 0, 0), name=f'truth{i}')

    # show_object(assy, name='assy')
    tmp = assy.toCompound()
    exporters.export(tmp, "mesh.stl")


if __name__ == '__main__':
    mu = [1., 4.]
    m = montecarlo_test(1, mu, same_vel=True, use_split=True)
    # test = m[-1]
    # case = test[-1]
    # cad_associtation(case['asso'], case['tracks'], case['truths'])
