# Track Association

Implementation eines Track-to-Track Assozoations Algorithmus (T2TA). Algorithmus stammt aus dem Paper "Track-to-track
association based on stochastic optimization" von L. M. Wolf, S. Steuernagel, K. Thormann, and M. Baum. Das Paper ist unter folgendem Link zu finden: https://ieeexplore.ieee.org/document/10224113. 
