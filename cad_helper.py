import cadquery as cq
import numpy as np
import math

"""
This file contains helper function to construct shapes with CadQuery
"""

def get_circle(c):
    """
    This Method generates a circle with center c
    """
    cir = cq.Workplane("XY").circle(0.3) # create circle with radius 0.3 in the origin
    cir = cir.extrude(1).translate((c[0], c[1], 0)) # first extrude the circle so it isn't flat anymore, then translate it to c
    return cir


def get_rect(c, rot=0):
    """
    This Method generates a circle with center c and rotates by angle rot
    """
    x, y, l = c # the center contains also the side lenght of the rectangle one side lenght is always 2
    res = cq.Workplane("XY").box(l, 2, 0.5).rotate((0, 0, 1), (0, 0, 0), rot) # create a box and rotate it around the z axis by angle rot
    res = res.translate((x, y, 0)) # move the rectangle to the center point
    return res


def get_poly(pts):
    """
    This Method generates a polygon specified by the given pts. The polygon is also extruded so it isn't flat.
    """
    return cq.Workplane("XY").polyline(pts).close().extrude(0.5)


def contains(c1, c2):
    """
    This Method checks if two circles overlap. They are specified by c1 and c2. Both contain the coordinates of the center points and the radius (3. component)
    """
    d = math.sqrt((c1[0] - c2[0]) * (c1[0] - c2[0]) + (c1[1] - c2[1]) * (c1[1] - c1[1])) # calc distance between centers
    return d < (c1[2] // 2 + c2[2] // 2) + 0.5 # check if distance is smaller than radius

def get_angle(v1, v2):
    """
    This Method calcs an angle between two given vectors
    """
    PI = np.pi
    # calc angle difference
    a = np.arctan2(v1[1], v1[0]) - np.arctan2(v2[1], v2[0])
    # normalize angles
    if a > PI:
        a -= 2 * PI
    elif a <= -PI:
        a += 2 * PI

    return a * 180 / PI
