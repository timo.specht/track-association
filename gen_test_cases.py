import numpy as np
import random
from cad_helper import contains
"""
This file provides different methods to generate test cases for the association algo
"""

def get_center_length(cnt_center):
    """
    This method generates a given number of non overlapping cicles
    """
    res = []
    i = 0
    while i < cnt_center:
        # generate center point  +  radius 
        x1 = random.randint(0, 10) * 5
        y1 = random.randint(0, 10) * 5
        l1 = random.randint(3, 5)
        tmp = np.array([x1, y1, l1])
        # check if the generated circle intersects with any circle that is already in our list
        if any([contains(c, tmp) for c in res]):
            continue
        # if no overlap is given we add the circle and increment the amount of generated circles
        res.append(tmp)
        i += 1
    return np.array(res)


def gen_centers(cnt_centers, mu, sigma=np.eye(2), scale=5):
    """
    This method generates a given number of points. 
    """
    # Draw the centers from a probability distribuiton with specified mean = mu and covariance = sigma
    centers = np.random.multivariate_normal(mu, sigma, size=cnt_centers) * scale
    # add two extra coordinates for the velocity of the objects
    centers = np.hstack((centers, [[0, 1] for _ in range(cnt_centers)]))
    return centers


def gen_center_hist(cnt_centers, cnt_points, mu):
    """
    This method generates a moving objects. The number of steps is determined by cnt_points
    """
    # first we generate our original centers
    centers = gen_centers(cnt_centers, mu)
    # the original centers are the first part
    res, tmp_centers = [centers], []

    old_centers = centers.copy()
    # generate initial velocity
    vs = [np.random.multivariate_normal(mean=[0, 0], cov=np.eye(2)) for _ in range(cnt_centers)]
    for i in range(cnt_points):
        tmp_centers = []
        for c, v in zip(old_centers, vs):
            # every step we move the objects by adding the velocity and saving the current velocity in the last to coordinates
            v += np.random.multivariate_normal(mean=[0, 0], cov=np.eye(2))
            c[:2] += v # move objects
            c[-2:] = v # save velocity
            tmp_centers.append(c)
        old_centers = np.array(tmp_centers)
        res.append(old_centers)  # append new centers to result

    return res


def gen_dynamic_2(center_hist, cnt_sensors, sigma_sensor=np.eye(2)):
    res = []
    for cs in center_hist:
        tracks = gen_tracks(cs, cnt_sensors, sigma_sensor=sigma_sensor)
        res.append((tracks, cs))

    return res


# für jedes center rufe gen_tracks auf

def gen_static(centers, cnt_sensors, sigma_sensor=np.eye(2)):
    """
    This method gnerates for given centers the tracks and return them. This is used for the static scenario
    Could be deleted as the static scenario is just the dynamic scenario with one step
    """
    tracks = gen_tracks(centers, cnt_sensors, sigma_sensor=sigma_sensor)
    return tracks, centers


def gen_tracks(centers, cnt_sensors, sigma_sensor=np.eye(2)):
    """
    This method actually generates the tracks
    """
    cnt_centers = len(centers)
    tracks = []
    # do the following for all sensors
    for i in range(cnt_sensors):
        # get white noise
        noise = np.random.multivariate_normal(mean=[0., 0.], cov=sigma_sensor, size=cnt_centers)

        # add noise to data
        ts = centers[:, :2] + noise

        # append sensor id to track
        ts = np.hstack((ts, [[i + 1] for _ in range(cnt_centers)]))
        # append center id to track
        ts = np.hstack((ts, [[j + 1] for j in range(cnt_centers)]))
        tracks.extend(ts)

    return np.array(tracks)


def gen_dynamic(centers, cnt_sensors, cnt_points, sigma_sensor=np.eye(2)):
    """
    Generate the tracks for a dynamic scenario. Depricated?
    """
    # initial tracks without any movement
    cnt_centers = len(centers)
    tracks = gen_tracks(centers, cnt_sensors, sigma_sensor=sigma_sensor)
    res, tmp_centers = [(tracks, centers)], []
    old_centers = centers.copy()
    # generate initial velocity
    vs = [np.random.multivariate_normal(mean=[0, 0], cov=np.eye(2)) for _ in range(cnt_centers)]
    for i in range(cnt_points):
        tmp_centers = []
        for c, v in zip(old_centers, vs):
            # add noise to velocity
            v += np.random.multivariate_normal(mean=[0, 0], cov=np.eye(2))
            c[:2] += v
            c[-2:] = v
            tmp_centers.append(c)
        old_centers = np.array(tmp_centers)
        tracks = gen_tracks(old_centers, cnt_sensors, sigma_sensor=sigma_sensor)
        res.append((tracks, old_centers))

    return res


if __name__ == '__main__':
    cs = gen_centers(3, [0., 0.])
    print(len(cs))
    bla = gen_dynamic_2(cs, 5,)
    for t in bla:
        print(t)
        print('----------------------------------')
    # print(gen_tracks(4, 4, [0, 0])[1])
